using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
public class ReadOnlyPropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        GUI.enabled = false;
        GUI.color = Color.green;
        EditorGUI.PropertyField(position, property, label);
        GUI.color = Color.white;
        GUI.enabled = true;
    }
}
