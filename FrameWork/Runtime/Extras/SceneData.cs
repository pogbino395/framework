using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(menuName = "Scene/SceneData")]
public class SceneData : ScriptableObject
{
    [SerializeField] public SceneInfo sceneInfo;
    [SerializeField] public SceneType sceneType;

#if UNITY_EDITOR
    protected void OnValidate()
    {
        if(!sceneInfo.isSceneAsset)
        {
            if(!sceneInfo.isEmpty)
                sceneInfo.ResetScene();
        }
        else
        {
            sceneInfo.SetSceneName(sceneInfo.SceneObject.name);
        }
    }
#endif
}

[System.Serializable]
public struct SceneInfo
{
    [SerializeField] private Object scene;
    [HideInInspector] public string sceneName;

#if UNITY_EDITOR
    public bool isSceneAsset => scene != null && scene is SceneAsset;
    public bool isEmpty => scene == null;
#endif

    public void ResetScene()
    {
        scene = null;
        sceneName = "";
    }

    public void SetSceneName(string name)
    {
        sceneName = name;
    }

    public Object SceneObject => scene;
    public string SceneName { get { return sceneName != null && sceneName != "" ? sceneName : scene.name; } }
}

public enum SceneType
{
    MENU,
    GAME,
    TUTORIAL
}
