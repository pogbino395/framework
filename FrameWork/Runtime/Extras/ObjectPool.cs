using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ObjectPool
{
    [SerializeField] public GameObject prefab = null;
    [SerializeField] public int quantity = 0;
    [SerializeField] public bool shouldIncrease = false;

    private Queue<GameObject> objects = new Queue<GameObject>();

    public int prefabID { get; private set; }

    public void Initialize(Transform parent)
    {
        prefabID = prefab.GetInstanceID();

        for (int i = 0; i < quantity; i++)
        {
            GameObject gameObject = UnityEngine.Object.Instantiate(prefab);
            gameObject.SetActive(false);
            gameObject.transform.parent = parent;
            objects.Enqueue(gameObject);
        }
    }

    public GameObject Pull(GameObject gameObjectToPool)
    {
        if (objects.Count > 0)
        {
            gameObjectToPool = objects.Dequeue();
            return gameObjectToPool;
        }

        if (shouldIncrease)
        {
            gameObjectToPool = UnityEngine.Object.Instantiate(prefab);
            return gameObjectToPool;
        }

        gameObjectToPool = null;
        return gameObjectToPool;
    }

    public void Return(GameObject objectToPushBackToPool, Transform parent)
    {
        objectToPushBackToPool.transform.parent = parent;
        objects.Enqueue(objectToPushBackToPool);
        objectToPushBackToPool.SetActive(false);
    }
}

