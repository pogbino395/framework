using UnityEngine;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    public static void Save(string saveFileName, SaveClass saveClass)
    {
        BinaryFormatter formatter = GetFormatter();

        string path = Path.Combine(Application.persistentDataPath, saveFileName + ".save");

        FileStream stream = new FileStream(path, FileMode.Create);

        formatter.Serialize(stream, saveClass);
        stream.Close();
    }

    public static SaveClass Load(string saveFileName)
    {
        BinaryFormatter formatter = GetFormatter();

        string path = Path.Combine(Application.persistentDataPath, saveFileName + ".save");

        FileStream stream = new FileStream(path, FileMode.OpenOrCreate);

        if (stream.Length > 0)
        {
            SaveClass save = formatter.Deserialize(stream) as SaveClass;
            stream.Close();
            return save;
        }
        else
        {
            stream.Close();
            return null;
        }
    }

    public static BinaryFormatter GetFormatter()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        SurrogateSelector selector = new SurrogateSelector();

        Vector3Surrogate v3Sur = new Vector3Surrogate();
        QuaternionSurrogate quatSur = new QuaternionSurrogate();

        selector.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All), v3Sur);
        selector.AddSurrogate(typeof(Quaternion), new StreamingContext(StreamingContextStates.All), quatSur);

        formatter.SurrogateSelector = selector;
        return formatter;
    }
}
