using System;
using UnityEngine;

public class Sorting<T> where T : IComparable<T>
{
    public T[] SelectionSort(T[] array, int length) //N^2
    {
        for (int i = 0; i < length - 1; i++)
        {
            int smallestVal = i;

            for (int j = i + 1; j < length; j++)
            {
                if (array[j].CompareTo(array[smallestVal]) < 0) 
                    smallestVal = j;
            }

            T tempVar = array[smallestVal];
            array[smallestVal] = array[i];
            array[i] = tempVar;
        }

        return array;
    }

    public T[] InsertionSort(T[] array, int length) //N^2
    {
        for (int i = 1; i < length; i++)
        {
            T key = array[i];
            int index = 0;

            for (int j = i - 1; j >= 0 && index != 1;)
            {
                if (key.CompareTo(array[j]) < 0) 
                {
                    array[j + 1] = array[j];
                    j--;
                    array[j + 1] = key;
                }
                else
                    index = 1;
            }
        }

        return array;
    }

    public T[] MergeSort(T[] array, int left, int right) //N log(N)
    {
        if (left < right)
        {
            int middle = left + (right - left) / 2;

            MergeSort(array, left, middle);
            MergeSort(array, middle + 1, right);

            MergeArray(array, left, middle, right);
        }

        return array;
    }

    private void MergeArray(T[] array, int left, int middle, int right)
    {
        int leftArrayLength = middle - left + 1;
        int rightArrayLength = right - middle;
        T[] leftTempArray = new T[leftArrayLength];
        T[] rightTempArray = new T[rightArrayLength];
        int i, j;

        for (i = 0; i < leftArrayLength; ++i)
            leftTempArray[i] = array[left + i];
        for (j = 0; j < rightArrayLength; ++j)
            rightTempArray[j] = array[middle + 1 + j];

        i = 0;
        j = 0;
        int k = left;

        while (i < leftArrayLength && j < rightArrayLength)
        {
            if (leftTempArray[i].CompareTo(rightTempArray[j]) <= 0) 
                array[k++] = leftTempArray[i++];
            else
                array[k++] = rightTempArray[j++];
        }

        while (i < leftArrayLength)
            array[k++] = leftTempArray[i++];

        while (j < rightArrayLength)
            array[k++] = rightTempArray[j++];
    }
}