using System;
using UnityEngine;
using Microsoft.CSharp;
public class Algorithms : MonoBehaviour
{
    public void Sort<T>(AlgorithmType type, T[] array, Sorting<T> sorting) where T : IComparable<T>
    {
        switch (type)
        {
            case AlgorithmType.INSERTION:
                array = sorting.InsertionSort(array, array.Length);
                break;
            case AlgorithmType.SELECTION:
                array = sorting.SelectionSort(array, array.Length);
                break;
            case AlgorithmType.MERGE:
                array = sorting.MergeSort(array, 0, array.Length - 1);
                break;
            default: break;
        }
    }
}

[System.Serializable]
public struct StructSort<T> : IComparable<StructSort<T>>
{
    [SerializeField] T value;
    [SerializeField] public int priority;

    public int CompareTo(StructSort<T> other)
    {
        if (other == null)
            return 1;

        if (this == other)
            return 0;
        else
            return this < other ? -1 : 1;
    }


    public static bool operator <(StructSort<T> one, StructSort<T> two)
    {
        return (one.priority < two.priority);
    }
    public static bool operator >(StructSort<T> one, StructSort<T> two)
    {
        return (one.priority < two.priority);
    }
    public static bool operator <=(StructSort<T> one, StructSort<T> two)
    {
        return (one.priority < two.priority);
    }
    public static bool operator >=(StructSort<T> one, StructSort<T> two)
    {
        return (one.priority < two.priority);
    }
    public static bool operator ==(StructSort<T> one, StructSort<T> two)
    {
        return Equals(one.priority, two.priority);
    }
    public static bool operator !=(StructSort<T> one, StructSort<T> two)
    {
        return !Equals(one.priority, two.priority);
    }
    public override bool Equals(object obj)
    {
        return base.Equals(obj);
    }
    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
    public override string ToString()
    {
        return "Value: " + value.ToString() + " priority: " + priority;
    }
}

[System.Serializable]
public class ClassSort<T> : IComparable<ClassSort<T>>
{
    [SerializeField] public T value;
    [SerializeField] public int priority;

    public int CompareTo(ClassSort<T> other)
    {
        if (other == null)
            return 1;

        if (this == other)
            return 0;
        else
            return this < other ? -1 : 1;
    }

    public static bool operator <(ClassSort<T> one, ClassSort<T> two)
    {
        return (one.priority < two.priority);
    }
    public static bool operator >(ClassSort<T> one, ClassSort<T> two)
    {
        return (one.priority < two.priority);
    }
    public static bool operator <=(ClassSort<T> one, ClassSort<T> two)
    {
        return (one.priority < two.priority);
    }
    public static bool operator >=(ClassSort<T> one, ClassSort<T> two)
    {
        return (one.priority < two.priority);
    }
    public static bool operator ==(ClassSort<T> one, ClassSort<T> two)
    {
        return equals(one, two);
    }
    public static bool operator !=(ClassSort<T> one, ClassSort<T> two)
    {
        return !equals(one, two);
    }
    public override bool Equals(object obj)
    {
        return base.Equals(obj);
    }
    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
    public override string ToString()
    {
        return "Value: " + value.ToString() + " priority: " + priority; 
    }
    private static bool equals(ClassSort<T> one, ClassSort<T> two)
    {
        if (one is null)
            return two is null;
        else if (two is null)
            return one is null;

        return (one.priority == two.priority && (dynamic)one.value == (dynamic)two.value);
    }
}

public enum AlgorithmType
{
    INSERTION,
    SELECTION,
    MERGE
}
