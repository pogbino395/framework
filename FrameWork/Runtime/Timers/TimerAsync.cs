using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace UsefulTool.Timers
{
    public class TimerAsync
    {
        private float currentTime = 0.0f;
        private int loopCounter = 0;
        private bool isTimerRunning = false;
        private bool shouldLoop = false;
        private bool isTimerJustStarted = true;
        private float runInterval = 0.0f;
        private float startDelay = 0.0f;
        private string debugMessage = "";

        public delegate void TimerDelegate();
        private TimerDelegate delegateAction;

        CancellationTokenSource cts;

        public async void StartTimer(TimerDelegate timerDelegate, float interval, bool loop = false, float startDelay = 0.0f, string debugMessage = "")
        {
            currentTime = 0.0f;
            loopCounter = 0;
            isTimerRunning = true;
            runInterval = interval;
            shouldLoop = loop;
            delegateAction = timerDelegate;
            this.startDelay = startDelay;
            cts = new CancellationTokenSource();

            while (isTimerRunning && !cts.Token.IsCancellationRequested)
            {
                if (isTimerJustStarted)
                {
                    currentTime -= startDelay;
                    isTimerJustStarted = false;
                }
                currentTime += Time.deltaTime;

                if (currentTime >= runInterval)
                {
                    delegateAction();
                    if (shouldLoop)
                    {
                        currentTime = 0.0f;
                        loopCounter++;
                    }
                    else
                    {
                        StopTimer();
                        isTimerRunning = false;
                    }
                }

                await Task.Yield();

            }
        }

        public void StopTimer()
        {
            currentTime = 0.0f;
            loopCounter = 0;
            isTimerRunning = false;
            if (debugMessage != "")
                Debug.Log(debugMessage);
            if (cts != null)
                cts.Cancel();
        }

        public void UpdateInterval(float NewInterval)
        {
            if (isTimerRunning)
                runInterval = NewInterval;
        }

        public int GetLoopsNum() { return loopCounter; }

        public bool IsRunning() { return isTimerRunning; }

        public float StartTime() { return currentTime; }

    }
}