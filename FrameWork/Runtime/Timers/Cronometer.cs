﻿using System;

namespace UsefulTool.Timers
{
    public class Chronometer 
    {
        private DateTime startTime;

        public Chronometer()
        {
            StartTime();
        }

        public int timeMilliseconds { get { return (int)(DateTime.Now - startTime).TotalMilliseconds; } }
        public int timeSeconds { get { return timeMilliseconds / 1000; } }

        public void StartTime() { startTime = DateTime.Now; }
    }
}
