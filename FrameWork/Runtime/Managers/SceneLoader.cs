using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class SceneLoader : Singleton<SceneLoader>
{
    [SerializeField] private SceneData mainMenu;
    [SerializeField] private Animator transition;
    [SerializeField] private int transitionTime;

    public void LoadMainMenu()
    {
        LoadScene(mainMenu);
    }

    public void LoadScene(SceneData scene)
    {
        StartCoroutine(LoadSceneAnimation(scene.sceneInfo.SceneName));
    }

    public void ReloadScene()
    {
        StartCoroutine(LoadSceneAnimation(SceneManager.GetActiveScene().name));
    }

    private IEnumerator LoadSceneAnimation(string sceneName)
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene(sceneName);
    }
}
