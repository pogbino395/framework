using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UsefulTool.Timers;

public class GameManager : Singleton<GameManager>
{
    private List<string> saveFileNames = new List<string>();
    public string currentSaveFileName { get; private set; } = "FrameWork";

    private SaveClass data;

    protected override void Awake()
    {
        base.Awake();

        LoadGame();
    }

    public void AddNewSaveFile(string fileName)
    {
        int index = saveFileNames.Count;
        saveFileNames.Add(fileName + index.ToString() + ".save");
    }

    public void ChangeCurrentSaveFile(int index)
    {
        if (index >= saveFileNames.Count)
        {
            Debug.LogError("Index out of bound, save file does not exist!");
        }
        else
        {
            currentSaveFileName = saveFileNames[index];
        }
    }

    public void LoadGame()
    {
        data = SaveSystem.Load(currentSaveFileName);
        if (data == null)
        {
            data = new SaveClass();

            SaveGame();
        }
    }

    public void SaveGame()
    {
        SaveSystem.Save(currentSaveFileName, data);
    }
}
