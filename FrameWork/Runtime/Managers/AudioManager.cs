using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : Singleton<AudioManager>
{
    [SerializeField] AudioSource musicSource;
    [SerializeField] List<AudioSource> sfxSource;
    [SerializeField] GameObject audioSource;

    public void PlaySfx(AudioClip clip)
    {
        bool soundPlayed = false;
        foreach (AudioSource source in sfxSource)
        {
            if (!source.isPlaying)
            {
                source.PlayOneShot(clip);
                soundPlayed = true;
                break;
            }
        }
        if (!soundPlayed)
        {
            AudioSource newSource = PoolManager.Instance.GetObject(audioSource, Vector3.zero, Quaternion.identity).GetComponent<AudioSource>();
            sfxSource.Add(newSource);
            newSource.PlayOneShot(clip);
            StartCoroutine(EndSound(clip, newSource));
        }
    }

    private IEnumerator EndSound(AudioClip audioClip, AudioSource audioSource)
    {
        float duration = 0;

        while (duration < audioClip.length)
        {
            duration += Time.deltaTime;
            yield return null;
        }

        PoolManager.Instance.ReturnToPool(audioSource.gameObject);
        sfxSource.Remove(audioSource);
    }

    public void PlayMusic(AudioClip musicClip)
    {
        if (musicSource.isPlaying)
        {
            musicSource.Stop();
        }
        musicSource.clip = musicClip;
        musicSource.Play();
    }
}
