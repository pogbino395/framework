using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    [SerializeField] public bool mobileInput = false;

#region PC
    private Controls inputControls;

    private void Awake()
    {
        if(!mobileInput)
        {
            inputControls = new Controls();
            Init();
        }
    }

    private void OnEnable()
    {
        if (!mobileInput)
        {
            inputControls.InputControls.Enable();
        }
    }

    private void OnDisable()
    {
        if (!mobileInput)
        {
            inputControls.InputControls.Disable();
        }
    }

    private void Init()
    {
        if (!mobileInput)
        {
            inputControls.InputControls.Quit.performed += act => Quit();
        }
    }

    private void Quit()
    {
        Application.Quit();
    }
    #endregion

#region Mobile

    [ShowIf(nameof(mobileInput), true, DisablingType.DONTSHOW)]
    [SerializeField] private float swipeDistance = 1f;

    private float timer = 0f;
    private float lastTouchTimer = 0f;
    private Vector2 startPosition;
    private Vector2 firstTouchPos;
    private Vector2 secondTouchPos;
    private bool moving = false;
    private bool doubleTap = false;

    private void Start()
    {
        if (mobileInput)
        {
            Input.simulateMouseWithTouches = true;
        }
    }

    void Update()
    {
        if (mobileInput)
        {
            if (Input.touchCount == 1)
            {
                BasicGestures();
            }
            else if (Input.touchCount == 2)
            {
                Pinch();
            }
        }
    }

    private void BasicGestures()
    {
        Touch touchInput = Input.GetTouch(0);

        if (touchInput.phase == TouchPhase.Began)
        {
            startPosition = touchInput.position;
            timer = 0f;
            float timeLastTouch = Time.time - lastTouchTimer;

            if (timeLastTouch < 0.2f)
            {
                DoubleTap();
                doubleTap = true;
            }
            else
            {
                doubleTap = false;
            }

            lastTouchTimer = Time.time;
        }

        if (touchInput.phase == TouchPhase.Ended)
        {
            if (!moving)
            {
                if (Vector2.Distance(touchInput.position, startPosition) > swipeDistance)
                {
                    if (Vector3.Dot(Vector2.up, (touchInput.position - startPosition).normalized) >= 0.5f || Vector3.Dot(Vector2.up, (touchInput.position - startPosition).normalized) <= -0.5f)
                    {
                        if (Vector3.Dot(Vector2.up, (touchInput.position - startPosition).normalized) >= 0.5f)
                        {
                            SwipeUp();
                        }
                        else
                        {
                            SwipeDown();
                        }
                    }
                    else if (Vector3.Dot(Vector2.right, (touchInput.position - startPosition).normalized) >= 0.5f || Vector3.Dot(Vector2.right, (touchInput.position - startPosition).normalized) <= -0.5f)
                    {
                        if (Vector3.Dot(Vector2.right, (touchInput.position - startPosition).normalized) >= 0.5f)
                        {
                            SwipeRight();
                        }
                        else
                        {
                            SwipeLeft();
                        }
                    }
                }
                else
                {
                    if (!doubleTap)
                    {
                        Tap();
                    }
                }
            }
            else
            {
                moving = false;
            }
        }

        if (touchInput.phase == TouchPhase.Stationary && timer > 0.2f)
        {
            timer = 0f;
            moving = true;
        }

        if (moving)
        {
            Drag(touchInput.position);
        }

        timer += Time.deltaTime;
    }

    private void Pinch()
    {
        Touch firstTouch = Input.GetTouch(0);
        Touch secondTouch = Input.GetTouch(1);

        firstTouchPos = firstTouch.position - firstTouch.deltaPosition;
        secondTouchPos = secondTouch.position - secondTouch.deltaPosition;

        float prevDifference = (firstTouchPos - secondTouchPos).magnitude;
        float currentDifference = (firstTouch.position - secondTouch.position).magnitude;

        float zoom = (firstTouch.deltaPosition - secondTouch.deltaPosition).magnitude * Time.deltaTime;

        if (prevDifference > currentDifference)
        {
            ZoomOut(zoom);
        }
        else
        {
            ZoomIn(zoom);
        }

        Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, 2f, 10f);
    }

    private void Tap()
    {
        EventManager.tap.Invoke();
    }

    private void DoubleTap()
    {
        EventManager.doubleTap.Invoke();
    }

    private void ZoomOut(float zoom)
    {
        EventManager.zoomOut.Invoke(zoom);
    }

    private void ZoomIn(float zoom)
    {
        EventManager.zoomIn.Invoke(zoom);
    }

    private void SwipeLeft()
    {
        EventManager.swipeLeft.Invoke();
    }

    private void SwipeRight()
    {
        EventManager.swipeRight.Invoke();
    }

    private void SwipeUp()
    {
        EventManager.swipeUp.Invoke();
    }

    private void SwipeDown()
    {
        EventManager.swipeDown.Invoke();    
    }

    private void Drag(Vector3 position)
    {
        EventManager.drag.Invoke(position);
    }
#endregion
}
