using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : Singleton<PoolManager>
{
    [SerializeField] private List<ObjectPool> prefabsPoolData;
    private Dictionary<int, ObjectPool> dictionaryObjects = new Dictionary<int, ObjectPool>();
    private Dictionary<GameObject, int> holderID = new Dictionary<GameObject, int>();

    protected override void Awake()
    {
        base.Awake();
        SpawnPools();
    }

    private void SpawnPools()
    {
        for (int i = 0; i < prefabsPoolData.Count; i++)
        {
            prefabsPoolData[i].Initialize(transform);
            dictionaryObjects.Add(prefabsPoolData[i].prefabID, prefabsPoolData[i]);
        }
    }

    public GameObject GetObject(GameObject prefab)
    {
        GameObject pooled = GetGameObject(prefab);
        if (pooled != null)
        {
            pooled.transform.parent = null;
            pooled.SetActive(true);
        }
        return pooled;
    }

    public GameObject GetObject(GameObject prefab, Vector3 position)
    {
        GameObject pooled = GetGameObject(prefab);
        if (pooled != null)
        {
            pooled.transform.parent = null;
            pooled.transform.localPosition = position;
            pooled.SetActive(true);
        }
        return pooled;
    }

    public GameObject GetObject(GameObject prefab, Vector3 position, Quaternion rotation)
    {
        GameObject pooled = GetGameObject(prefab);
        if (pooled != null)
        {
            pooled.transform.parent = null;
            pooled.transform.localPosition = position;
            pooled.transform.localRotation = rotation;
            pooled.SetActive(true);
        }
        return pooled;
    }

    private GameObject GetGameObject(GameObject prefab)
    {
        GameObject pooled = null;
        int prefabID = prefab.GetInstanceID();

        if (!dictionaryObjects.ContainsKey(prefabID))
        {
            pooled = Instantiate(prefab);
            holderID.Add(pooled, prefabID);
        }
        else
        {
            pooled = dictionaryObjects[prefabID].Pull(pooled);
            if (pooled != null && !holderID.ContainsKey(pooled))
                holderID.Add(pooled, prefabID);
        }
        return pooled;
    }

    public void ReturnToPool(GameObject prefab)
    {
        int prefabID = holderID[prefab];
        dictionaryObjects[prefabID].Return(prefab, transform);
    }
}

