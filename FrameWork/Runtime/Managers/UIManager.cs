using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UsefulTool.Timers;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    [SerializeField] private GameObject winScreen;
    [SerializeField] private GameObject loseScreen;

    public void WinGame()
    {
        winScreen.SetActive(true);
    }

    public void LoseGame()
    {
        loseScreen.SetActive(true);
    }
}
