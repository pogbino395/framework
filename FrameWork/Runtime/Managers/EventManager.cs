using System;
using System.Collections.Generic;
using UnityEngine;

public static class EventManager
{
    public static Action tap;

    public static Action doubleTap;

    public static Action<float> zoomOut;

    public static Action<float> zoomIn;

    public static Action swipeDown;

    public static Action swipeUp;

    public static Action swipeLeft;

    public static Action swipeRight;

    public static Action<Vector3> drag;
}
