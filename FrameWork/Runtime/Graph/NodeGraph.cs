using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class NodeGraph<T>
{
    public T value { get; private set; }
    public List<Edge<T>> edges = new List<Edge<T>>();

    public NodeGraph(T value)
    {
        this.value = value;
    }

    public void AddEgde(NodeGraph<T> target, int weight = 0, int height = 0)
    {
        edges.Add(new Edge<T>(this, target, weight, height));
    }
    
    public void RemoveEdge(NodeGraph<T> node)
    {
        for(int i = edges.Count - 1; i >= 0; i--)
        {
            if(edges[i].endNodeGraph == node || edges[i].endNodeGraph.value.Equals(node.value))
            {
                edges.Remove(edges[i]);
                break;
            }
        }
    }
}

public sealed class Edge<T>
{
    public NodeGraph<T> startNodeGraph;
    public NodeGraph<T> endNodeGraph;
    public int weight;
    public int height;

    public Edge(NodeGraph<T> start, NodeGraph<T> end, int weight = 0, int height = 0)
    {
        this.startNodeGraph = start;
        this.endNodeGraph = end;
        this.weight = weight;
        this.height = height;
    }
}
