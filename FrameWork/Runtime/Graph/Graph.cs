using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public sealed class Graph<T>
{
    public List<NodeGraph<T>> nodeGraphs = new List<NodeGraph<T>>();

    public Dictionary<NodeGraph<T>, Vector3> positionNode = new Dictionary<NodeGraph<T>, Vector3>();

    private List<NodeGraph<T>> pathTile = new List<NodeGraph<T>>();

    private const float distance = .7f;

    public void AddNode(T value, Vector3 position)
    {
        NodeGraph<T> nodeToAdd = new NodeGraph<T>(value);
        nodeGraphs.Add(nodeToAdd);
        positionNode.Add(nodeToAdd, position);
    }

    public void AddEdges(T value, List<T> neighbors = null, List<int> weights = null, List<int> heights = null, List<bool> walkable = null)
    {
        if(nodeGraphs.Count != 0)
        {
            if (neighbors == null)
                return;

            NodeGraph<T> tempStart = null;

            foreach (var var in nodeGraphs)
            {
                if (var.value.Equals(value))
                {
                    tempStart = var;
                    break;
                }
            }

            if (tempStart == null)
                return;

            for (int i = 0; i < neighbors.Count; i++)
            {
                NodeGraph<T> tempEnd = null;

                for (int j = 0; j < nodeGraphs.Count; j++)
                {
                    if (nodeGraphs[j].value.Equals(neighbors[i]))
                    {
                        tempEnd = nodeGraphs[j];
                        break;
                    }
                }

                if (tempEnd == null)
                    continue;

                if (walkable[i])
                    tempStart.AddEgde(tempEnd, weights[i], heights[i]);
            }
        }
    }

    public void RemoveNode(T value)
    {
        NodeGraph<T> temp = null;

        for (int i = 0; i < nodeGraphs.Count; i++)
        {
            if (nodeGraphs[i].value.Equals(value))
            {
                temp = nodeGraphs[i];
                break;
            }
        }
        for (int i = 0; i < temp.edges.Count; i++)
        {
            temp.edges[i].endNodeGraph.RemoveEdge(temp);
        }

        nodeGraphs.Remove(temp);
        positionNode.Remove(temp);
    }

    public HashSet<NodeGraph<T>> CheckReachableTiles(T node, int potential = 0, int jump = 0)
    {
        pathTile = new List<NodeGraph<T>>();

        NodeGraph<T> temp = null;

        Dictionary<NodeGraph<T>, int> potentialNode = new Dictionary<NodeGraph<T>, int>();
        Queue<NodeGraph<T>> queue = new Queue<NodeGraph<T>>();
        HashSet<NodeGraph<T>> visited = new HashSet<NodeGraph<T>>();

        for (int i = 0; i < nodeGraphs.Count; i++)
        {
            if (nodeGraphs[i].value.Equals(node))
                temp = nodeGraphs[i];

            potentialNode.Add(nodeGraphs[i], int.MaxValue);
        }

        potentialNode[temp] = 0;
        queue.Enqueue(temp);

        while (queue.Count != 0)
        {
            temp = queue.Dequeue();
            visited.Add(temp);
            pathTile.Add(temp);

            for (int i = 0; i < temp.edges.Count; i++)
            {
                if (jump < temp.edges[i].height)
                    continue;

                if (potentialNode[temp] + temp.edges[i].weight <= potential)
                {
                    potentialNode[temp.edges[i].endNodeGraph] = temp.edges[i].weight + potentialNode[temp];

                    if (!visited.Contains(temp.edges[i].endNodeGraph) && !queue.Contains(temp.edges[i].endNodeGraph))
                        queue.Enqueue(temp.edges[i].endNodeGraph);
                }
            }
        }

        return visited;
    }

    public void PrintDFS(NodeGraph<T> node)
    {
        HashSet<NodeGraph<T>> visited = new HashSet<NodeGraph<T>>();
        PrintDFS(node, ref visited);
    }

    private void PrintDFS(NodeGraph<T> node, ref HashSet<NodeGraph<T>> visited)
    {
        Debug.Log(node.value);
        visited.Add(node);

        for (int i = 0; i < node.edges.Count; i++)
        {
            if (!visited.Contains(node.edges[i].endNodeGraph))
                PrintDFS(node.edges[i].endNodeGraph, ref visited);
        }
    }

    public Queue<NodeGraph<T>> Dijkstra(T start, T end, int jump = 0)
    {
        NodeGraph<T> tempStart = null;
        NodeGraph<T> tempEnd = null;

        HashSet<NodeGraph<T>> toVisit = new HashSet<NodeGraph<T>>();
        HashSet<NodeGraph<T>> visited = new HashSet<NodeGraph<T>>();
        Dictionary<NodeGraph<T>, int> potentialNode = new Dictionary<NodeGraph<T>, int>();
        Dictionary<NodeGraph<T>, NodeGraph<T>> prevNode = new Dictionary<NodeGraph<T>, NodeGraph<T>>();

        for (int i = 0; i < nodeGraphs.Count; i++)
        {
            if (nodeGraphs[i].value.Equals(start))
                tempStart = nodeGraphs[i];
            if (nodeGraphs[i].value.Equals(end))
                tempEnd = nodeGraphs[i];

            potentialNode.Add(nodeGraphs[i], int.MaxValue);
            prevNode.Add(nodeGraphs[i], null);
        }

        potentialNode[tempStart] = 0;
        toVisit.Add(tempStart);

        while (toVisit.Count > 0)
        {
            NodeGraph<T> tempNode = ClosestNode(potentialNode, toVisit);

            if (tempNode == tempEnd)
                break;

            for (int i = 0; i < tempNode.edges.Count; i++)
            {
                if (jump < tempNode.edges[i].height || !pathTile.Contains(tempNode.edges[i].endNodeGraph))
                    continue;

                if (potentialNode[tempNode] + tempNode.edges[i].weight < potentialNode[tempNode.edges[i].endNodeGraph])
                {
                    potentialNode[tempNode.edges[i].endNodeGraph] = tempNode.edges[i].weight + potentialNode[tempNode];
                    prevNode[tempNode.edges[i].endNodeGraph] = tempNode;

                    toVisit.Add(tempNode.edges[i].endNodeGraph);
                }
            }

            toVisit.Remove(tempNode);
            visited.Add(tempNode);
        }

        NodeGraph<T> tempNodePath = tempEnd;
        Dictionary<NodeGraph<T>, NodeGraph<T>> tempPrevNode = new Dictionary<NodeGraph<T>, NodeGraph<T>>();

        while (tempNodePath != tempStart)
        {
            if (prevNode.ContainsKey(tempNodePath))
            {
                tempPrevNode[tempNodePath] = prevNode[tempNodePath];
                tempNodePath = prevNode[tempNodePath];
            }
        }

        prevNode = tempPrevNode;

        Queue<NodeGraph<T>> path = new Queue<NodeGraph<T>>();

        foreach (var var in prevNode.Reverse())
        {
            path.Enqueue(var.Value);
        }

        path.Enqueue(tempEnd);

        return path;
    }

    private NodeGraph<T> ClosestNode(Dictionary<NodeGraph<T>, int> potentialNode, HashSet<NodeGraph<T>> toVisit)
    {
        int currentPot = int.MaxValue;
        NodeGraph<T> temp = null;

        foreach (var var in potentialNode)
        {
            if (var.Value < currentPot && toVisit.Contains(var.Key))
            {
                currentPot = var.Value;
                temp = var.Key;
            }
        }
        return temp;
    }

    public Queue<NodeGraph<T>> AStar(T start, T end, int jump = 0)
    {
        NodeGraph<T> tempStart = null;
        NodeGraph<T> tempEnd = null;

        List<NodeGraph<T>> toVisit = new List<NodeGraph<T>>();
        HashSet<NodeGraph<T>> visited = new HashSet<NodeGraph<T>>();

        Dictionary<NodeGraph<T>, int> potentialNode = new Dictionary<NodeGraph<T>, int>();
        Dictionary<NodeGraph<T>, int> heuristicNode = new Dictionary<NodeGraph<T>, int>();
        Dictionary<NodeGraph<T>, NodeGraph<T>> prevNode = new Dictionary<NodeGraph<T>, NodeGraph<T>>();

        for (int i = 0; i < nodeGraphs.Count; i++)
        {
            if (nodeGraphs[i].value.Equals(start))
                tempStart = nodeGraphs[i];
            if (nodeGraphs[i].value.Equals(end))
                tempEnd = nodeGraphs[i];

            potentialNode.Add(nodeGraphs[i], int.MaxValue);
            heuristicNode.Add(nodeGraphs[i], 0);
            prevNode.Add(nodeGraphs[i], null);
        }

        potentialNode[tempStart] = 0;
        toVisit.Add(tempStart);
        heuristicNode[tempStart] = GetDistance(tempStart, tempEnd);

        while (toVisit.Count > 0)
        {
            NodeGraph<T> tempNode = null;

            for(int i = 0; i < toVisit.Count; i++)
            {
                if (tempNode == null || potentialNode[toVisit[i]] + heuristicNode[toVisit[i]] < potentialNode[tempNode] + heuristicNode[tempNode])
                    tempNode = toVisit[i];
            }

            if (tempNode == tempEnd)
                break;

            for(int i = 0; i < tempNode.edges.Count; i++)
            {
                if (jump < tempNode.edges[i].height || visited.Contains(tempNode.edges[i].endNodeGraph) || !pathTile.Contains(tempNode.edges[i].endNodeGraph))
                    continue;

                int valueTot = potentialNode[tempNode] + tempNode.edges[i].weight * GetDistance(tempNode.edges[i].endNodeGraph, tempEnd);

                if (!toVisit.Contains(tempNode.edges[i].endNodeGraph) || valueTot < potentialNode[tempNode])
                {
                    heuristicNode[tempNode.edges[i].endNodeGraph] = tempNode.edges[i].weight * GetDistance(tempNode.edges[i].endNodeGraph, tempEnd);
                    potentialNode[tempNode.edges[i].endNodeGraph] = tempNode.edges[i].weight + potentialNode[tempNode];
                    prevNode[tempNode.edges[i].endNodeGraph] = tempNode;

                    if (!toVisit.Contains(tempNode.edges[i].endNodeGraph))
                        toVisit.Add(tempNode.edges[i].endNodeGraph);
                }
            }

            toVisit.Remove(tempNode);
            visited.Add(tempNode);
        }

        NodeGraph<T> tempNodePath = tempEnd;
        Dictionary<NodeGraph<T>, NodeGraph<T>> tempPrevNode = new Dictionary<NodeGraph<T>, NodeGraph<T>>();

        while (tempNodePath != tempStart)
        {
            if (prevNode.ContainsKey(tempNodePath))
            {
                tempPrevNode[tempNodePath] = prevNode[tempNodePath];
                tempNodePath = prevNode[tempNodePath];
            }
        }

        prevNode = tempPrevNode;

        Queue<NodeGraph<T>> path = new Queue<NodeGraph<T>>();

        foreach (var var in prevNode.Reverse())
        {
            path.Enqueue(var.Value);
        }

        path.Enqueue(tempEnd);

        return path;
    }

    private int GetDistance(NodeGraph<T> startNode, NodeGraph<T> endNode)
    {
        float distanceX = Mathf.Abs(positionNode[startNode].x - positionNode[endNode].x);
        float distanceZ = Mathf.Abs(positionNode[startNode].z - positionNode[endNode].z);

        int returnValue = (int)(distanceX + distanceZ);

        return returnValue;
    }
}
