using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UsefulTool.Tweener
{
    public abstract class TweenerMain : MonoBehaviour
    {
        protected enum LerpModeEnum
        {
            ONCE,
            BUTTON,
            PING_PONG,
            PING_PONG_REPEAT
        }
        [SerializeField]
        [Tooltip("Set if the animation should play when the object is enabled.")]
        protected bool PlayOnEnable = false;
        [SerializeField]
        [Tooltip("Set if the animation should play when the TimeScale is set to zero.")]
        protected bool PlayOnGamePaused = false;
        [SerializeField]
        [Tooltip("If the animation should be terminated on disable.")]
        protected bool stopMoveOnDisable = false;
        [SerializeField]
        [Tooltip("Desired curve for the animation.")]
        private AnimationCurve easeCurve;
        [SerializeField]
        [Tooltip("Choose the animation mode.\n ONCE: lerps between the two fixed vectors.\n BUTTON: if called again, the animation is inverted.\n PING PONG: goes back and forth between the vectors once.\n PING PONG REPEAT: what do you think it does Sherlock?")]
        protected LerpModeEnum LerpMode = LerpModeEnum.ONCE;
        [SerializeField]
        [Tooltip("Choose if the animation should be based on time or speed.")]
        LerpType LerpType;
        [SerializeField]
        [Tooltip("The desired duration or speed of the animation.")]
        protected float value = 1;

        protected float timeElapsed;
        protected float speedMultiplier = 1;
        private float defaultSpeed;

        private bool isSpeedUp = false;

        [SerializeField]
        public UnityEvent LerpEndTrigger;
        [SerializeField]
        [Tooltip("Event triggered when the animation is played in reverse.")]
        public UnityEvent LerpEndTriggerReverse;

        private int loopCounter = 0;

        protected float animationTimePosition;

        /// <summary>
        /// Updates the starting point of the animation
        /// </summary>
        public virtual void UpdatePath()
        {
            // to be overrided
        }

        /// <summary>
        /// Called by the child position component, lerps between two Vector3 variables changing the position of the object
        /// </summary>
        /// <param name="startPoint">The starting vector of the animation</param>
        /// <param name="target">The destination vector the lerp should reach</param>
        /// <param name="isReversed">If the lerp should run in reverse mode</param>
        /// <returns></returns>
        protected bool SmoothLerpPosition(in Vector3 startPoint, in Vector3 target, bool isReversed)
        {
            bool isAnimationFinished = false;
            switch (LerpType)
            {
                case LerpType.TIME:
                    if (timeElapsed < value)
                    {
                        if (PlayOnGamePaused)
                            timeElapsed += Time.unscaledDeltaTime;
                        else
                            timeElapsed += Time.deltaTime;
                        transform.position = Vector3.Lerp(startPoint, target, easeCurve.Evaluate(timeElapsed / value));
                    }
                    else
                    {
                        transform.position = target;
                        timeElapsed = 0;
                        isAnimationFinished = true;

                        if (isReversed)
                        {
                            if (LerpEndTriggerReverse != null)
                                LerpEndTriggerReverse.Invoke();
                        }
                        else
                        {
                            if (LerpEndTrigger != null)
                                LerpEndTrigger.Invoke();
                        }
                       
                    }
                    break;
                case LerpType.SPEED:
                    if (!AlmostEqualVector3(target, transform.position))
                    {
                        if (PlayOnGamePaused)
                            animationTimePosition += Time.unscaledDeltaTime * value;
                        else
                            animationTimePosition += Time.deltaTime * value;
                        transform.position = Vector3.Lerp(startPoint, target, easeCurve.Evaluate(animationTimePosition));
                    }
                    else
                    {
                        transform.position = target;
                        animationTimePosition = 0;
                        if (LerpEndTrigger != null)
                            LerpEndTrigger.Invoke();
                        isAnimationFinished = true;
                    }
                    break;
            }
            return isAnimationFinished;
        }

        /// <summary>
        /// Called by the child scale component, lerps between two Vector3 variables changing the scale of the object
        /// </summary>
        /// <param name="startPoint">The starting vector of the animation</param>
        /// <param name="target">The destination vector the lerp should reach</param>
        /// <param name="isReversed">If the lerp should run in reverse mode</param>
        /// <returns></returns>
        protected bool SmoothLerpScale(in Vector3 startPoint, in Vector3 target, bool isReversed)
        {
            bool isAnimationFinished = false;
            switch (LerpType)
            {
                case LerpType.TIME:
                    if (timeElapsed < value)
                    {
                        if (PlayOnGamePaused)
                            timeElapsed += Time.unscaledDeltaTime;
                        else
                            timeElapsed += Time.deltaTime;
                        transform.localScale = Vector3.Lerp(startPoint, target, easeCurve.Evaluate(timeElapsed / value));
                    }
                    else
                    {
                        transform.localScale = target;
                        timeElapsed = 0;
                        if (isReversed)
                        {
                            if (LerpEndTriggerReverse != null)
                                LerpEndTriggerReverse.Invoke();
                        }
                        else
                        {
                            if (LerpEndTrigger != null)
                                LerpEndTrigger.Invoke();
                        }
                        isAnimationFinished = true;
                    }
                    break;
                case LerpType.SPEED:
                    if (!AlmostEqualVector3(target, transform.localScale, 0.00001f))
                    {
                        if (PlayOnGamePaused)
                            animationTimePosition += Time.unscaledDeltaTime * value;
                        else
                            animationTimePosition += Time.deltaTime * value;
                        transform.localScale = Vector3.Lerp(startPoint, target, easeCurve.Evaluate(animationTimePosition));
                    }
                    else
                    {
                        transform.localScale = target;
                        animationTimePosition = 0;
                        if (LerpEndTrigger != null)
                            LerpEndTrigger.Invoke();
                        isAnimationFinished = true;
                    }
                    break;
            }
            return isAnimationFinished;
        }

        /// <summary>
        /// Called by the child scale component, lerps between two Quaternion variables changing the scale of the object
        /// </summary>
        /// <param name="startPoint">The starting Quaternion of the animation</param>
        /// <param name="target">The destination Quaternion the lerp should reach</param>
        /// <param name="isReversed">If the lerp should run in reverse mode</param>
        /// <returns></returns>
        protected bool SmoothLerpRotation(in Quaternion startRotation, in Quaternion target, bool isReversed, bool useLocalRotation)
        {
            bool isAnimationFinished = false;
            switch (LerpType)
            {
                case LerpType.TIME:
                    if (timeElapsed < value)
                    {
                        if (PlayOnGamePaused)
                            timeElapsed += Time.unscaledDeltaTime;
                        else
                            timeElapsed += Time.deltaTime;
                        if (!useLocalRotation)
                            transform.rotation = Quaternion.Lerp(startRotation, target, easeCurve.Evaluate(timeElapsed / value));
                        else
                            transform.localRotation = Quaternion.Lerp(startRotation, target, easeCurve.Evaluate(timeElapsed / value));
                    }
                    else
                    {
                        if (!useLocalRotation)
                            transform.rotation = target;
                        else
                            transform.localRotation = target;
                        timeElapsed = 0;
                        if (isReversed)
                        {
                            if (LerpEndTriggerReverse != null)
                                LerpEndTriggerReverse.Invoke();
                        }
                        else
                        {
                            if (LerpEndTrigger != null)
                                LerpEndTrigger.Invoke();
                        }
                        isAnimationFinished = true;
                    }
                    break;
                case LerpType.SPEED:
                    if (target != transform.rotation)
                    {
                        if (PlayOnGamePaused)
                            animationTimePosition += Time.unscaledDeltaTime * value;
                        else
                            animationTimePosition += Time.deltaTime * value;
                        transform.rotation = Quaternion.Lerp(startRotation, target, easeCurve.Evaluate(animationTimePosition));
                    }
                    else
                    {
                        animationTimePosition = 0;
                        if (LerpEndTrigger != null)
                            LerpEndTrigger.Invoke();
                        isAnimationFinished = true;
                    }
                    break;
            }
            return isAnimationFinished;
        }

        /// <summary>
        /// Called by the child position component, lerps between two Vector3 variables changing the position of the object
        /// </summary>
        /// <param name="startPoint">The starting vector of the animation</param>
        /// <param name="target">The destination vector the lerp should reach</param>
        /// <param name="isReversed">If the lerp should run in reverse mode</param>
        /// <returns></returns>
        protected bool SmoothLerpAlpha(in float startPoint, in float target, Image image, bool isReversed)
        {
            bool isAnimationFinished = false;

            var tempColor = image.color;

            switch (LerpType)
            {
                case LerpType.TIME:
                    if (timeElapsed < value)
                    {
                        if (PlayOnGamePaused)
                            timeElapsed += Time.unscaledDeltaTime;
                        else
                            timeElapsed += Time.deltaTime;
                        tempColor.a = Mathf.Lerp(startPoint, target, easeCurve.Evaluate(timeElapsed / value));
                        image.color = tempColor;
                    }
                    else
                    {
                        tempColor.a = target;
                        image.color = tempColor;
                        //UpdatePath();
                        timeElapsed = 0;
                        if (isReversed)
                        {
                            if (LerpEndTriggerReverse != null)
                                LerpEndTriggerReverse.Invoke();
                        }
                        else
                        {
                            if (LerpEndTrigger != null)
                                LerpEndTrigger.Invoke();
                        }
                        isAnimationFinished = true;
                    }
                    break;
                case LerpType.SPEED:
                    if (target != image.color.a)
                    {
                        if (PlayOnGamePaused)
                            animationTimePosition += Time.unscaledDeltaTime * value;
                        else
                            animationTimePosition += Time.deltaTime * value;
                        tempColor.a = Mathf.Lerp(startPoint, target, easeCurve.Evaluate(animationTimePosition));
                        image.color = tempColor;
                    }
                    else
                    {
                        tempColor.a = target;
                        image.color = tempColor;
                        animationTimePosition = 0;
                        if (LerpEndTrigger != null)
                            LerpEndTrigger.Invoke();
                        isAnimationFinished = true;
                    }
                    break;
            }
            return isAnimationFinished;
        }

        /// <summary>
        /// Switches between two input floats, used for reversed animations
        /// </summary>
        /// <param name="floatOne">The first float</param>
        /// <param name="floatTwo">The second float</param>
        protected void InvertFloats(ref float floatOne, ref float floatTwo)
        {
            float tempFloat = floatOne;
            floatOne = floatTwo;
            floatTwo = tempFloat;
        }

        /// <summary>
        /// Switches between two input vectors, used for reversed animations
        /// </summary>
        /// <param name="VectorOne">The first vector</param>
        /// <param name="VectorTwo">The second vector</param>
        protected void InvertVectors(ref Vector3 VectorOne, ref Vector3 VectorTwo)
        {
            Vector3 TempVector = VectorOne;
            VectorOne = VectorTwo;
            VectorTwo = TempVector;
        }

        /// <summary>
        /// Switches between two input quaternions, used for reversed animations
        /// </summary>
        /// <param name="QuaternionOne">The first quaternion</param>
        /// <param name="QuaternionTwo">The second quaternion</param>
        protected void InvertQuaternions(ref Quaternion QuaternionOne, ref Quaternion QuaternionTwo)
        {
            Quaternion TempQuaternion = QuaternionOne;
            QuaternionOne = QuaternionTwo;
            QuaternionTwo = TempQuaternion;
        }

        /// <summary>
        /// Optional debug print
        /// </summary>
        protected virtual void DebugPrint(string print)
        {
            Debug.Log(print);
        }

        public static bool AlmostEqualVector3(Vector3 lhs, Vector3 rhs, float difference = 0.5f)
        {
            return (double)Vector3.SqrMagnitude(lhs - rhs) < difference;
        }

        public void SetSpeedMultiplier(float multiplier)
        {
            isSpeedUp = true;
            defaultSpeed = value;
            speedMultiplier = multiplier;

            if (LerpType == LerpType.SPEED)
            {
                value *= speedMultiplier;
            }
        }

        public void RestoreDefaultSpeed()
        {
            if (LerpType == LerpType.SPEED && isSpeedUp)
            {
                value = defaultSpeed;
                isSpeedUp = false;
            }
        }
    }
}