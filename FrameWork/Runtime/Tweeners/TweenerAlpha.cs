using UnityEngine;
using UnityEngine.UI;

namespace UsefulTool.Tweener
{
    public sealed class TweenerAlpha : TweenerMain
    {
        [SerializeField]
        [Tooltip("Custom alpha start point. For values < 0 the current image alpha will be used.")]
        private float startPoint = -1;
        [SerializeField]
        [Tooltip("Target alpha value of the animation.")]
        private float target;
        [SerializeField]
        [Tooltip("If the animation should start from scratch every time the component is enabled.")]
        private bool resetAnimationOnEnable = false;

        private bool shouldChangeAlpha = false;
        private bool isAnimationInactive = true;
        private bool doOnce = true;
        private int loopCounter_PingPong = 0;
        private bool isAnimationReversed = false;

        private Image image;

        private void Awake()
        {
            if (TryGetComponent<Image>(out image)) { }
            else
                Debug.Log("Image component not found. This Tweener needs an image component to function.");
        }

        private void OnEnable()
        {
            UpdatePath();
            if (resetAnimationOnEnable)
                timeElapsed = 0;
            if (PlayOnEnable)
                ActivateMovement();
        }


        void Start()
        {
            UpdatePath();
        }

        /// <summary>
        /// Switches between the selected lerp mode and calls the lerp function
        /// </summary>
        void LateUpdate()
        {
            if (shouldChangeAlpha)
            {
                switch (LerpMode)
                {
                    case LerpModeEnum.ONCE:
                        isAnimationInactive = SmoothLerpAlpha(startPoint, target, image, false);
                        if (isAnimationInactive && doOnce)
                        {
                            DeactivateMovement();
                            doOnce = false;
                            //print("Animation finished");
                        }
                        break;
                    case LerpModeEnum.BUTTON:
                        isAnimationInactive = SmoothLerpAlpha(startPoint, target, image, isAnimationReversed);
                        if (isAnimationInactive && doOnce)
                        {
                            DeactivateMovement();
                            InvertFloats(ref startPoint, ref target);
                            doOnce = false;
                            isAnimationReversed = !isAnimationReversed;
                            //print("Animation finished");
                        }
                        break;
                    case LerpModeEnum.PING_PONG:
                        if (loopCounter_PingPong < 2)
                        {
                            isAnimationInactive = SmoothLerpAlpha(startPoint, target, image, isAnimationReversed);
                            if (isAnimationInactive)
                                doOnce = true;
                            if (isAnimationInactive && doOnce)
                            {
                                InvertFloats(ref startPoint, ref target);
                                doOnce = false;
                                isAnimationReversed = !isAnimationReversed;
                                loopCounter_PingPong++;
                                //print("Animation finished");
                            }
                        }
                        else
                        {
                            loopCounter_PingPong = 0;
                            DeactivateMovement();
                        }
                        break;
                    case LerpModeEnum.PING_PONG_REPEAT:
                        isAnimationInactive = SmoothLerpAlpha(startPoint, target, image, isAnimationReversed);
                        if (isAnimationInactive)
                            doOnce = true;
                        if (isAnimationInactive && doOnce)
                        {
                            //DeactivateMovement();
                            InvertFloats(ref startPoint, ref target);
                            isAnimationReversed = !isAnimationReversed;
                            doOnce = false;
                            //print("Animation finished");
                        }
                        break;
                }
            }
        }

        private void OnDisable()
        {
            if (stopMoveOnDisable)
                DeactivateMovement();
        }

        /// <summary>
        /// Updates the starting position for the animation
        /// </summary>
        public override void UpdatePath() 
        { 
            if (startPoint < 0)
                startPoint = image.color.a;
            else
            {
                var tempColor = image.color;
                tempColor.a = startPoint;
                image.color = tempColor;
            }
        }

        /// <summary>
        /// Activates the animation with the possibility of overriding the animation target (to be used mainly via script)
        /// </summary>
        /// <param name="targetOverride"></param>
        public void ActivateMovement(float targetOverride = default)
        {
            target = targetOverride;
            loopCounter_PingPong = 0;
            shouldChangeAlpha = true;
            doOnce = true;
        }

        /// <summary>
        /// Activates the animation (to be attached to events)
        /// </summary>
        public void ActivateMovement() { shouldChangeAlpha = true; doOnce = true; loopCounter_PingPong = 0;}

        /// <summary>
        /// Immediately stops the movement
        /// </summary>
        public void DeactivateMovement() { shouldChangeAlpha = false; }

        /// <summary>
        /// Immediately stops the movement with the possibility to reset the animation
        /// </summary>
        public void DeactivateMovement(bool shouldResetAnimation)
        {
            shouldChangeAlpha = false;
            if (shouldResetAnimation)
            {
                timeElapsed = 0;
                animationTimePosition = 0;
            }
        }

        /// <summary>
        /// Call to set a new target position for the animation
        /// </summary>
        /// <param name="newTarget">The desired new target</param>
        public void SetTarget(float newTarget)
        {
            target = newTarget;
        }

    }
}