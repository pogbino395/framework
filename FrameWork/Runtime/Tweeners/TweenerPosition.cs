using UnityEngine;
using UnityEngine.Events;

namespace UsefulTool.Tweener
{
    public sealed class TweenerPosition : TweenerMain
    {
        [SerializeField]
        [Tooltip("Tick this setting if the destination position in input is relative to the object's parent.")]
        private bool isPositionRelative = false;
        [SerializeField]
        [Tooltip("Target vector of the animation.")]
        private Vector3 target;
        //[SerializeField]
        private Vector3 startPoint;
        [SerializeField]
        [Tooltip("The position of the object referred here will be used as destination for the animation. Ignored if NULL.")]
        public Transform targetTransform;
        [SerializeField]
        [Tooltip("If the animation should start from scratch every time the component is enabled.")]
        private bool resetAnimationOnEnable = false;
        [SerializeField]
        [Tooltip("Resets the starting position on each call. Useful to concatenate animations.")]
        private bool updateStartPosition = false;
        [SerializeField]
        public UnityEvent AutoTrigger;

        private bool shouldMove = false;
        private bool isAnimationInactive = true;
        private bool doOnce = true;
        private int loopCounter_PingPong = 0;
        private bool isAnimationReversed = false;

        private void OnEnable()
        {
            if (resetAnimationOnEnable)
                timeElapsed = 0;
            if (PlayOnEnable)
                ActivateMovement();
        }


        void Start()
        {
            UpdatePath();
            if (isPositionRelative & targetTransform == null)
                target = transform.TransformPoint(target);
            else if (targetTransform != null)
                target = targetTransform.position;
        }

        /// <summary>
        /// Switches between the selected lerp mode and calls the lerp function
        /// </summary>
        void LateUpdate()
        {
            if (shouldMove)
            {
                switch (LerpMode)
                {
                    case LerpModeEnum.ONCE:
                        isAnimationInactive = SmoothLerpPosition(startPoint, target, false);
                        if (isAnimationInactive && doOnce)
                        {
                            DeactivateMovement();
                            doOnce = false;
                            //print("Animation finished");
                            AutoTrigger?.Invoke();
                            AutoTrigger.RemoveAllListeners();
                        }
                        break;
                    case LerpModeEnum.BUTTON:
                        isAnimationInactive = SmoothLerpPosition(startPoint, target, isAnimationReversed);
                        if (isAnimationInactive && doOnce)
                        {
                            DeactivateMovement();
                            InvertVectors(ref startPoint, ref target);
                            doOnce = false;
                            isAnimationReversed = !isAnimationReversed;
                            //print("Animation finished");
                            AutoTrigger?.Invoke();
                            AutoTrigger.RemoveAllListeners();
                        }
                        break;
                    case LerpModeEnum.PING_PONG:
                        if (loopCounter_PingPong < 2)
                        {
                            isAnimationInactive = SmoothLerpPosition(startPoint, target, isAnimationReversed);
                            if (isAnimationInactive)
                                doOnce = true;
                            if (isAnimationInactive && doOnce)
                            {
                                InvertVectors(ref startPoint, ref target);
                                doOnce = false;
                                isAnimationReversed = !isAnimationReversed;
                                loopCounter_PingPong++;
                                //print("Animation finished");
                            }
                        }
                        else
                        {
                            loopCounter_PingPong = 0;
                            DeactivateMovement();
                        }
                        break;
                    case LerpModeEnum.PING_PONG_REPEAT:
                        isAnimationInactive = SmoothLerpPosition(startPoint, target, isAnimationReversed);
                        if (isAnimationInactive)
                            doOnce = true;
                        if (isAnimationInactive && doOnce)
                        {
                            //DeactivateMovement();
                            InvertVectors(ref startPoint, ref target);
                            isAnimationReversed = !isAnimationReversed;
                            doOnce = false;
                            //print("Animation finished");
                        }
                        break;
                }
            }
        }

        private void OnDisable()
        {
            if (stopMoveOnDisable)
                DeactivateMovement();
        }

        /// <summary>
        /// Updates the starting position for the animation
        /// </summary>
        public override void UpdatePath() { startPoint = transform.position; }

        /// <summary>
        /// Activates the animation with the possibility of overriding the animation target (to be used mainly via script)
        /// </summary>
        /// <param name="targetOverride"></param>
        public void ActivateMovement(Vector3 targetOverride = default)
        {
            if (updateStartPosition)
                UpdatePath();
            target = targetOverride;
            loopCounter_PingPong = 0;
            shouldMove = true;
            doOnce = true;
        }

        /// <summary>
        /// Activates the animation (to be attached to events)
        /// </summary>
        public void ActivateMovement() 
        { 
            shouldMove = true; doOnce = true; loopCounter_PingPong = 0; if (updateStartPosition) { UpdatePath(); } }

        /// <summary>
        /// Immediately stops the movement
        /// </summary>
        public void DeactivateMovement() 
        { 
            shouldMove = false; 
        }

        /// <summary>
        /// Immediately stops the movement with the possibility to reset the animation
        /// </summary>
        public void DeactivateMovement(bool shouldResetAnimation)
        {
            shouldMove = false;
            if (shouldResetAnimation)
            {
                timeElapsed = 0;
                animationTimePosition = 0;
            }
        }

        /// <summary>
        /// Call to set a new target position for the animation
        /// </summary>
        /// <param name="newTarget">The desired new target</param>
        public void SetTarget(Vector3 newTarget)
        {
            target = newTarget;
        }

        /// <summary>
        /// Call to set a new target position for the animation, transform overload
        /// </summary>
        /// <param name="newTarget">The desired new target</param>
        public void SetTarget(Transform newTarget)
        {
            target = newTarget.position;
        }
    }
}