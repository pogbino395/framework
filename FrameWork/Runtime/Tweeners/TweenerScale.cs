using UnityEngine;

namespace UsefulTool.Tweener
{
    public sealed class TweenerScale : TweenerMain
    {
        [SerializeField]
        [Tooltip("Target vector of the animation.")]
        private Vector3 targetScale;
        [SerializeField]
        [Tooltip("Target vector of the animation.")]
        private bool startAtScaleZero;
        [SerializeField]
        private float startDelay;
        //[SerializeField]
        private Vector3 startScale;

        private bool shouldRescale = false;
        private bool isAnimationInactive = false;
        private bool doOnce = true;
        private int loopCounter_PingPong = 0;
        private bool isAnimationReversed = false;

        private void Awake()
        {
            if (startAtScaleZero)
                gameObject.transform.localScale = Vector3.zero;
        }

        private void OnEnable()
        {
            
            if (PlayOnEnable)
                ActivateMovement();
        }


        void Start()
        {
            UpdatePath();
        }

        /// <summary>
        /// Switches between the selected lerp mode and calls the lerp function
        /// </summary>
        void LateUpdate()
        {
            if (shouldRescale)
            {
                switch (LerpMode)
                {
                    case LerpModeEnum.ONCE:
                        if (startDelay <= 0)
                        {
                            isAnimationInactive = SmoothLerpScale(startScale, targetScale, false);
                        }
                        else
                            startDelay -= Time.deltaTime;

                        if (isAnimationInactive && doOnce)
                        {
                            DeactivateMovement();
                            doOnce = false;
                            //print("Animation finished");
                        }
                        break;
                    case LerpModeEnum.BUTTON:
                        if (startDelay <= 0)
                        {
                            isAnimationInactive = SmoothLerpScale(startScale, targetScale, isAnimationReversed);
                        }
                        else
                            startDelay -= Time.deltaTime;

                        if (isAnimationInactive && doOnce)
                        {
                            DeactivateMovement();
                            InvertVectors(ref startScale, ref targetScale);
                            doOnce = false;
                            isAnimationReversed = !isAnimationReversed;
                            //print("Animation finished");
                        }
                        break;
                    case LerpModeEnum.PING_PONG:
                        if (loopCounter_PingPong < 2)
                        {
                            isAnimationInactive = SmoothLerpScale(startScale, targetScale, isAnimationReversed);
                            if (isAnimationInactive)
                                doOnce = true;
                            if (isAnimationInactive && doOnce)
                            {
                                InvertVectors(ref startScale, ref targetScale);
                                doOnce = false;
                                isAnimationReversed = !isAnimationReversed;
                                loopCounter_PingPong++;
                                //print("Animation finished");
                            }
                        }
                        else
                        {
                            loopCounter_PingPong = 0;
                            DeactivateMovement();
                        }
                        break;
                    case LerpModeEnum.PING_PONG_REPEAT:
                        isAnimationInactive = SmoothLerpScale(startScale, targetScale, isAnimationReversed);
                        if (isAnimationInactive)
                            doOnce = true;
                        if (isAnimationInactive && doOnce)
                        {
                            //DeactivateMovement();
                            InvertVectors(ref startScale, ref targetScale);
                            doOnce = false;
                            isAnimationReversed = !isAnimationReversed;
                            //print("Animation finished");
                        }
                        break;
                }
            }
        }

        private void OnDisable()
        {
            if (stopMoveOnDisable)
                DeactivateMovement();
        }

        /// <summary>
        /// Updates the starting scale for the animation
        /// </summary>
        public override void UpdatePath() { startScale = transform.localScale; }

        /// <summary>
        /// Activates the animation (to be attached to events)
        /// </summary>
        public void ActivateMovement() { shouldRescale = true; doOnce = true; loopCounter_PingPong = 0; }

        /// <summary>
        /// Activates the animation with the possibility of overriding the animation target (to be used mainly via script)
        /// </summary>
        /// <param name="targetOverride"></param>
        public void ActivateMovement(Vector3 targetOverride = default)
        {
            targetScale = targetOverride;
            loopCounter_PingPong = 0;
            shouldRescale = true;
            doOnce = true;
        }

        /// <summary>
        /// Immediately stops the movement
        /// </summary>
        public void DeactivateMovement() { shouldRescale = false; }

        /// <summary>
        /// Immediately stops the movement with the possibility to reset the animation
        /// </summary>
        public void DeactivateMovement(bool shouldResetAnimation)
        {
            shouldRescale = false;
            if (shouldResetAnimation)
            {
                timeElapsed = 0;
                animationTimePosition = 0;
            }
        }

    }
}