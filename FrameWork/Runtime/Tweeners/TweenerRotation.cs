using UnityEngine;

namespace UsefulTool.Tweener
{
    public sealed class TweenerRotation : TweenerMain
    {
        [SerializeField]
        [Tooltip("Target vector of the animation in euler angles.")]
        private Vector3 target;
        private Quaternion targetQuat;
        private Quaternion startPoint;
        [SerializeField]
        [Tooltip("An optional object to copy the rotation from. Ignored if NULL.")]
        private Transform rotationToCopy;
        [SerializeField]
        private bool localRotation;
        [SerializeField]
        [Tooltip("Resets the starting rotation on each call. Useful to concatenate animations.")]
        private bool updateStartRotation = false;

        private bool shouldMove = false;
        private bool isAnimationInactive = true;
        private bool doOnce = true;
        private int loopCounter_PingPong = 0;
        private bool isAnimationReversed = false;

        private void OnEnable()
        {
            if (rotationToCopy != null)
                targetQuat = rotationToCopy.rotation;
            if (PlayOnEnable)
                ActivateMovement();
        }

        void Start()
        {
            UpdatePath();
        }

        /// <summary>
        /// Switches between the selected lerp mode and calls the lerp function
        /// </summary>
        void LateUpdate()
        {
            if (shouldMove)
            {
                switch (LerpMode)
                {
                    case LerpModeEnum.ONCE:
                        isAnimationInactive = SmoothLerpRotation(startPoint, targetQuat, false, localRotation);
                        if (isAnimationInactive && doOnce)
                        {
                            DeactivateMovement();
                            doOnce = false;
                            //print("Animation finished");
                        }
                        break;
                    case LerpModeEnum.BUTTON:
                        isAnimationInactive = SmoothLerpRotation(startPoint, targetQuat, isAnimationReversed, localRotation);
                        if (isAnimationInactive && doOnce)
                        {
                            DeactivateMovement();
                            InvertQuaternions(ref startPoint, ref targetQuat);
                            doOnce = false;
                            isAnimationReversed = !isAnimationReversed;
                            //print("Animation finished");
                        }
                        break;
                    case LerpModeEnum.PING_PONG:
                        if (loopCounter_PingPong < 2)
                        {
                            isAnimationInactive = SmoothLerpRotation(startPoint, targetQuat, isAnimationReversed, localRotation);
                            if (isAnimationInactive)
                                doOnce = true;
                            if (isAnimationInactive && doOnce)
                            {
                                InvertQuaternions(ref startPoint, ref targetQuat);
                                doOnce = false;
                                isAnimationReversed = !isAnimationReversed;
                                loopCounter_PingPong++;
                                //print("Animation finished");
                            }
                        }
                        else
                        {
                            loopCounter_PingPong = 0;
                            DeactivateMovement();
                        }
                        break;
                    case LerpModeEnum.PING_PONG_REPEAT:
                        isAnimationInactive = SmoothLerpRotation(startPoint, targetQuat, isAnimationReversed, localRotation);
                        if (isAnimationInactive)
                            doOnce = true;
                        if (isAnimationInactive && doOnce)
                        {
                            //DeactivateMovement();
                            InvertQuaternions(ref startPoint, ref targetQuat);
                            isAnimationReversed = !isAnimationReversed;
                            doOnce = false;
                            //print("Animation finished");
                        }
                        break;
                }
            }
        }

        private void OnDisable()
        {
            if (stopMoveOnDisable)
                DeactivateMovement();
        }

        /// <summary>
        /// Updates the starting rotation for the animation
        /// </summary>
        public override void UpdatePath() 
        {
            if (!localRotation)
                startPoint = transform.rotation;
            else
                startPoint = transform.localRotation;
        }

        /// <summary>
        /// Activates the animation (to be attached to events)
        /// </summary>
        public void ActivateMovement() { if (updateStartRotation) UpdatePath(); shouldMove = true; doOnce = true; loopCounter_PingPong = 0; targetQuat = Quaternion.Euler(target); }

        /// <summary>
        /// Activates the animation with the possibility of overriding the animation target (to be used mainly via script)
        /// </summary>
        /// <param name="targetOverride"></param>
        public void ActivateMovement(Vector3 targetOverride = default)
        {
            if (updateStartRotation)
                UpdatePath();
            target = targetOverride;
            targetQuat = Quaternion.Euler(target);
            loopCounter_PingPong = 0;
            shouldMove = true;
            doOnce = true;
        }

        /// <summary>
        /// Immediately stops the movement
        /// </summary>
        public void DeactivateMovement() { shouldMove = false; }

        /// <summary>
        /// Immediately stops the movement with the possibility to reset the animation
        /// </summary>
        public void DeactivateMovement(bool shouldResetAnimation)
        {
            shouldMove = false;
            if (shouldResetAnimation)
            {
                timeElapsed = 0;
                animationTimePosition = 0;
            }
        }

    }
}